var memoryCategorie;
var memoryLetterFoto;
var memoryTimer;
var memoryTijd = 0;
var beurt = 0;
var spelGestart = false;

var keuzeTimeout;
var toonTijd = 2;
var aantalGoed = 0;
var aantalKeuzes = 12;
var keuzeHash = new Hash();

var alfabetArray = [
    {letter: "A"}, {letter: "B"}, {letter: "C"}, {letter: "D"}, {letter: "E"}, {letter: "F"}, {letter: "G"},
    {letter: "H"}, {letter: "I"}, {letter: "J"}, {letter: "K"}, {letter: "L"}, {letter: "M"}, {letter: "N"},
    {letter: "O"}, {letter: "P"}, {letter: "Q"}, {letter: "R"}, {letter: "S"}, {letter: "T"}, {letter: "U"},
    {letter: "V"}, {letter: "W"}, {letter: "X"}, {letter: "Y"}, {letter: "Z"}
];

var dierenFotoArray = [
    {img: "img/dieren/aap.png", alt: "Aap"},
    {img: "img/dieren/beer.png", alt: "Aap"},
    {img: "img/dieren/gijt.png", alt: "Gijt"},
    {img: "img/dieren/hond.png", alt: "Hond"},
    {img: "img/dieren/kat.png", alt: "Kat"},
    {img: "img/dieren/kikker.png", alt: "Kikker"},
    {img: "img/dieren/kip.png", alt: "Kip"},
    {img: "img/dieren/koe.png", alt: "Koe"},
    {img: "img/dieren/olifant.png", alt: "Olifant"},
    {img: "img/dieren/paard.png", alt: "Paard"},
    {img: "img/dieren/varken.png", alt: "Varken"},
    {img: "img/dieren/vogel.png", alt: "Vogel"},
    {img: "img/dieren/pinguin.png", alt: "Pinguin"},
    {img: "img/dieren/schaap.png", alt: "Schaap"},
    {img: "img/dieren/lieveheersbeestje.png", alt: "Lieveheersbeestje"},
    {img: "img/dieren/wolf.png", alt: "Wolf"},
    {img: "img/dieren/bever.png", alt: "Bever"},
    {img: "img/dieren/vis.png", alt: "Vis"},
    {img: "img/dieren/vos.png", alt: "Vos"},
    {img: "img/dieren/leeuw.png", alt: "Leeuw"},
    {img: "img/dieren/schildpad.png", alt: "Schildpad"},
    {img: "img/dieren/panda.png", alt: "Panda"},
    {img: "img/dieren/koala.png", alt: "Koala"},
    {img: "img/dieren/eend.png", alt: "Eend"},
    {img: "img/dieren/zeehond.png", alt: "Zeehond"},
    {img: "img/dieren/muis.png", alt: "Muis"}
];

var computerFotoArray = [
    {img: "img/computer/android.png", alt: "Android"},
    {img: "img/computer/apple.png", alt: "Apple"},
    {img: "img/computer/cloud.png", alt: "Cloud"},
    {img: "img/computer/computer.png", alt: "Computer"},
    {img: "img/computer/controller.png", alt: "Controller"},
    {img: "img/computer/duke.png", alt: "Duke"},
    {img: "img/computer/dvd.png", alt: "DVD"},
    {img: "img/computer/firewall.png", alt: "Firewall"},
    {img: "img/computer/floppy.png", alt: "Floppy"},
    {img: "img/computer/hardeschijf.png", alt: "Hardeschijf"},
    {img: "img/computer/joystick.png", alt: "Joystick"},
    {img: "img/computer/laptop.png", alt: "Laptop"},
    {img: "img/computer/like.png", alt: "Like"},
    {img: "img/computer/monitor.png", alt: "Monitor"},
    {img: "img/computer/muis.png", alt: "Muis"},
    {img: "img/computer/php.png", alt: "php"},
    {img: "img/computer/printer.png", alt: "Printer"},
    {img: "img/computer/python.png", alt: "Python"},
    {img: "img/computer/router.png", alt: "Router"},
    {img: "img/computer/smartphone.png", alt: "Smartphone"},
    {img: "img/computer/tablet.png", alt: "Tablet"},
    {img: "img/computer/toetsenbord.png", alt: "Toetsenbord"},
    {img: "img/computer/tux.png", alt: "Tux"},
    {img: "img/computer/usbstick.png", alt: "USB stick"},
    {img: "img/computer/wifi.png", alt: "WIFI"},
    {img: "img/computer/windows.png", alt: "Windows"}
];

var memoryArray;

window.WebSocket = window.WebSocket || window.MozWebSocket;
var connection = new WebSocket('ws://127.0.0.1:1337');

connection.onmessage = function (message)
{
	// try to decode json (I assume that each message from server is json)
	try
	{
		var json = JSON.parse(message.data);
		
		var MsgType = json.MsgType;
		if (MsgType == 'request_highscores_response')
		{
			handleHighscoreData(json.Data);
		}
		else if (MsgType == 'add_score_response')
		{
			var result = json.Msg;
			if (result === true)
			{
				alert('Je score is met success opgeslagen.');
			}
			else
			{
				alert('Er is iets fout gegaan tijdens het opslaan van je score!');
			}
		}
	}
	catch (e)
	{
		alert('This doesn\'t look like a valid JSON: ' + message.data);
		return;
	}
	// handle incoming message
};

connection.onopen = function ()
{
	requestHighscoreData();
}

function handleHighscoreData(data)
{
	// Maak de tbody met id scoreBoardBody leeg.
	$("scoreBoardBody").empty();
	
	var plaats = 1;
	var scoreBoardBody = "";
	var dataArray = JSON.decode(JSON.stringify(data));
	if (dataArray.length > 0)
	{
		dataArray.each(function(value, key)
		{
			scoreBoardBody = scoreBoardBody + '<tr><td class="left">#' + plaats++ + '</td><td class="left">' + value.name + '</td><td class="center">' + value.time + '</td><td class="center">' + value.moves + '</td><td class="center">' + value.score + '</td></tr>';
		});
	}
	else
	{
		scoreBoardBody = '<tr><td colspan="5" class="center">Top 10 is leeg!</td></tr>';
	}
	$("scoreBoardBody").set('html', scoreBoardBody);
	
	// Maak de tfoot met id scoreBoardFoot leeg.
	$("scoreBoardFoot").empty();
	var scoreBoardFoot = '<tr><td colspan="5" class="center small">Laatste update om ' + new Date().format("%d-%m-%Y %H:%M:%S") + '</td></tr>';
	$("scoreBoardFoot").set('html', scoreBoardFoot);
}

function requestHighscoreData()
{
	var obj = { };
	obj.MsgType = 'request_highscores';
	obj.Msg = 'now';
	
	var JSONString = JSON.stringify(obj);
	
	if (connection.readyState === connection.OPEN)
	{
		connection.send(JSONString);
	}
}

function initSpel()
{
	enableSlider();
	enableStartKnop();
	enableCategorieRadio();
    disableCheatKnop();
    this.memoryTijd = 0;
	updateMemoryTijd(this.memoryTijd);
    this.beurt = 0;
	updateBeurt(this.beurt);
	this.aantalGoed = 0;
    updateSlider(this.toonTijd);
	this.spelGestart = false;
    stopMemoryTijd();
    clearTimeout(this.goedTimeout);
	clearTimeout(this.keuzeTimeout);
	this.keuzeHash.empty();
}

function startMemorySpel()
{
	disableSlider();
    disableCategorieRadio();
	disableStartKnop();
	enableCheatKnop();
    setCategorie();
    maakSpeelveld();
}

function maakSpeelveld()
{
    this.memoryArray = getRandomMemoryArray();

     // Haal een verwijzing naar content
    var content = document.getElementById("content");

    // Maak een <table> element
    var tabel = document.createElement("table");
    // Maak het class attribuut voor de tabel en voeg deze toe
    tabel.setAttribute("class", "speelveld");

    var teller = 0;

    // Maak alle rijen
    for (var j = 0; j < 5; j++)
    {
        // Maak een <tr> element
        var rij = document.createElement("tr");

        // Maak alle cellen
        for (var i = 0; i < 5; i++)
        {
            // Maak een <td> element
            var cel = document.createElement("td");
            
            if (this.memoryLetterFoto === "letter")
            {
                if (teller === 12)
                {
                    // Maak een <img> element
                    var celInhoud = document.createElement("img");
                    celInhoud.setAttribute("src", "img/leeg.png");
                    celInhoud.setAttribute("alt", "Leeg");
                    celInhoud.setAttribute("style", "cursor: auto;");
                    celInhoud.setAttribute("class", "celfoto");
                }
                else
                {
                    // Maak een <div> element
                    var celInhoud = document.createElement("div");
                    celInhoud.innerHTML = "*";
                    // Maak het onclick attribuut voor de cel en voeg deze toe
                    cel.setAttribute("onclick", "draaiLetter(\"kaart-" + teller + "\")");
                    celInhoud.setAttribute("class", "celletter");
                }
                celInhoud.setAttribute("id", "kaart-" + teller);
            }
            else
            {
                // Maak een <img> element
                var celInhoud = document.createElement("img");
                if (teller === 12)
                {
                    celInhoud.setAttribute("src", "img/leeg.png");
                    celInhoud.setAttribute("alt", "Leeg");
                    celInhoud.setAttribute("style", "cursor: auto;");
                }
                else
                {
                    celInhoud.setAttribute("src", "img/verborgen.png");
                    celInhoud.setAttribute("alt", "Verborgen");
                    // Maak het onclick attribuut voor de cel en voeg deze toe
                    cel.setAttribute("onclick", "draaiFoto(\"kaart-" + teller + "\")");
                }
                celInhoud.setAttribute("id", "kaart-" + teller);
                celInhoud.setAttribute("class", "celfoto");
            }

            // Voeg de celInhoud toe aan de cel
            cel.appendChild(celInhoud);
            // Maak het id attribuut voor de cel en voeg deze toe
            cel.setAttribute("id", "cel-" + i + "-" + j);
            
            // Voeg de cel toe aan het eind van de rij
            rij.appendChild(cel);
            teller++;
        }

        // Voeg een rij toe aan het eind van de tabel
        tabel.appendChild(rij);
    }

    // Voeg de tabel toe aan het einde van content
    content.appendChild(tabel);
}

function getRandomMemoryArray()
{
    var tempArray;
    
    switch(this.memoryCategorie)
    {
        case "Alfabet":
            tempArray = this.alfabetArray.shuffle();
            break;
        case "Dieren":
            tempArray = this.dierenFotoArray.shuffle();
            break;
        case "Computer":
            tempArray = this.computerFotoArray.shuffle();
            break;
        default:
            tempArray = this.dierenFotoArray.shuffle();
            break;
    }
	
    var selectieArray = new Array();
    
    for (var i = 0; i < 12; i++)
    {
        selectieArray.push(tempArray[i]);
    }
    
    var randomMemoryArray = selectieArray.append(selectieArray).shuffle();
    var midden = randomMemoryArray[12];
    randomMemoryArray[12] = "leeg";
    randomMemoryArray[24] = midden;
    return randomMemoryArray;
}

function draaiFoto(id)
{
    if (this.spelGestart === false)
    {
        this.spelGestart = true;
        startMemoryTijd();
    }
    
    var img = document.getElementById(id);
    // Alleen foto tonen als deze verborgen is
    if (img.src.contains("verborgen.png"))
    {
        if (this.keuzeHash.getLength() < 2)
        {
            toonKeuze(id);
            if (isKeuzeGelijk(img.src))
            {
                updateBeurt(1);
                this.keuzeHash.empty();
                clearTimeout(this.keuzeTimeout);
                toonFotoGoed();
            }
            else
            {
                this.keuzeHash.include(id, img.src);
                if (this.keuzeHash.getLength() === 2)
                {
                    toonFotoFout();
                }
            }

            if (this.keuzeHash.getLength() === 1)
            {
                this.keuzeTimeout = setTimeout(function(){verbergKeuzes();}, this.toonTijd * 1000);
            }
        }
    }
}

function draaiLetter(id)
{
    if (this.spelGestart === false)
    {
        this.spelGestart = true;
        startMemoryTijd();
    }
    var div = document.getElementById(id);
    // Alleen foto tonen als deze verborgen is
    if (div.innerHTML === "*")
    {
        if (this.keuzeHash.getLength() < 2)
        {
            toonKeuze(id);
            if (isKeuzeGelijk(div.innerHTML))
            {
                updateBeurt(1);
                this.keuzeHash.empty();
                clearTimeout(this.keuzeTimeout);
                toonFotoGoed();
            }
            else
            {
                this.keuzeHash.include(id, div.innerHTML);
                if (this.keuzeHash.getLength() === 2)
                {
                    toonFotoFout();
                }
            }

            if (this.keuzeHash.getLength() === 1)
            {
                this.keuzeTimeout = setTimeout(function(){verbergKeuzes();}, this.toonTijd * 1000);
            }
        }
    }
}

function toonKeuze(id)
{
    if (this.memoryLetterFoto === "letter")
    {
        var div = document.getElementById(id);
        div.innerHTML = this.memoryArray[getVakNummer(id)].letter;
        div.setAttribute("style", "cursor: auto;");
    }
    else
    {
        var img = document.getElementById(id);
        img.src = this.memoryArray[getVakNummer(id)].img;
        img.alt = this.memoryArray[getVakNummer(id)].alt;
        img.setAttribute("style", "cursor: auto;");
    }
}

function verbergKeuze(id)
{
    if (this.memoryLetterFoto === "letter")
    {
        var div = document.getElementById(id);
        div.innerHTML = "*";
        div.removeAttribute("style");
    }
    else
    {
        var img = document.getElementById(id);
        img.src = "img/verborgen.png";
        img.alt = "Verborgen";
        img.removeAttribute("style");
    }
}

function toonFotoGoed()
{
    this.aantalGoed++;
    var img = document.getElementById("kaart-12");
    img.src = "img/goed.png";
    img.alt = "Goed";
    if (this.aantalGoed === this.aantalKeuzes)
    {
        stopMemorySpel();
    }
}

function toonFotoFout()
{
    var img = document.getElementById("kaart-12");
    img.src = "img/fout.png";
    img.alt = "Fout";
}

function toonFotoGefeliciteerd()
{
    var img = document.getElementById("kaart-12");
    img.src = "img/gefeliciteerd.png";
    img.alt = "Gefeliciteerd";
}

function verbergKeuzes()
{
    updateBeurt(1);
    this.keuzeHash.each(function(value, key)
    {
        verbergKeuze(key);
    });
    this.keuzeHash.empty();
}

function isKeuzeGelijk(value)
{
    return this.keuzeHash.hasValue(value);
}

function getVakNummer(id)
{
    return id.split("-")[1];
}

function startMemoryTijd()
{
    this.memoryTimer = setInterval(function(){updateMemoryTijd(1);}, 1000);
}

function stopMemoryTijd()
{
    clearInterval(this.memoryTimer);
}

function resetMemorySpel()
{
	initSpel();
	window.addEvent("domready", function()
	{
		$("content").empty();
		$("content").set('html', '&nbsp;');
	});
}

function stopMemorySpel()
{
    this.spelGestart = false;
    stopMemoryTijd();
    clearTimeout(this.goedTimeout);
    toonFotoGefeliciteerd();
    var naaminvoer = confirm("Gefeliciteerd!\n\nJe hebt er " + this.beurt + " beurten en " + this.memoryTijd + " seconden over gedaan.\n\nWilt u uw naam invoeren?");
    if (naaminvoer === true)
    {
        naamInvoeren();
    }
}

function naamInvoeren()
{
    var naam = prompt("Naam: ", "");
    if (naam !== null && naam !== "")
    {
    	var userObj = { };
    	userObj.MsgType = 'add_score';
    	userObj.Name = naam;
    	userObj.Time = this.memoryTijd;
    	userObj.Moves = this.beurt;
    
    	var JSONString = JSON.stringify(userObj);
    	
    	if (connection.readyState === connection.OPEN)
		{
	    	sendScoreToJSON(JSONString);
    	}
		else
		{
	    	alert('Er is iets fout gegaan tijdens het opslaan van je score!');
    	}
    }
}

function sendScoreToJSON(json)
{
	connection.send(json);
}

function updateMemoryTijd(seconde)
{
    this.memoryTijd = this.memoryTijd + seconde;
    document.getElementById("speltijd").innerHTML = "Tijd: " + this.memoryTijd;
}

function updateBeurt(aantal)
{
    this.beurt = this.beurt + aantal;
    document.getElementById("beurt").innerHTML = "Beurt: " + this.beurt;
}

function updateSlider(value)
{
    this.toonTijd = value;
    document.getElementById("toontijd").innerHTML = this.toonTijd + " seconden";
}


function disableSlider()
{
    document.getElementById("slider").disabled = true;
}

function disableStartKnop()
{
    document.getElementById("startKnop").disabled = true;
}

function disableCheatKnop()
{
    document.getElementById("cheatKnop").disabled = true;
}

function disableCategorieRadio()
{
    document.getElementById("alfabetRadio").disabled = true;
    document.getElementById("dierenRadio").disabled = true;
    document.getElementById("computerRadio").disabled = true;
}

function enableSlider()
{
    document.getElementById("slider").disabled = false;
}

function enableStartKnop()
{
    document.getElementById("startKnop").disabled = false;
}

function enableCheatKnop()
{
    document.getElementById("cheatKnop").disabled = false;
}

function enableCategorieRadio()
{
    document.getElementById("alfabetRadio").disabled = false;
    document.getElementById("dierenRadio").disabled = false;
    document.getElementById("computerRadio").disabled = false;
}

function setCategorie()
{
    var radio = document.getElementsByName("categorie");
    for (var i = 0; i < radio.length; i++)
    {
        if (radio[i].checked)
        {
            this.memoryCategorie = radio[i].value;
            if (memoryCategorie === "Alfabet")
            {
                this.memoryLetterFoto = "letter";
            }
            else
            {
                this.memoryLetterFoto = "foto";
            }
        }
    }
}

function cheat()
{
    disableCheatKnop();
    clearTimeout(this.keuzeTimeout);
    
    for (var i = 0; i < 25; i++)
    {
        toonKeuze("kaart-" + i)
    }
    stopMemorySpel();
}
