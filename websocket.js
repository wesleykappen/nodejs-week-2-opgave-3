var WebSocketServer = require('websocket').server;
var http = require('http');

var fs = require("fs");
var file = "scores.db";
var exists = fs.existsSync(file);

if(!exists) {
  console.log("Creating DB file.");
  fs.openSync(file, "w");
}

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(file);
db.run("CREATE TABLE IF NOT EXISTS scores (name VARCHAR, time INT, moves INT, date DATE)");
db.close();

var clients = [ ];

var server = http.createServer(function(request, response) {
    // process HTTP request. Since we're writing just WebSockets server
    // we don't have to implement anything.
});
server.listen(1337, function() {
	console.log((new Date()) + "Server is listening on Port: 1337");
});

// create the server
wsServer = new WebSocketServer({
    httpServer: server
});

// WebSocket server
wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');
    
    var connection = request.accept(null, request.origin);
    	
    var index = clients.push(connection) - 1;
 
    console.log((new Date()) + ' Connection accepted.');

    // This is the most important callback for us, we'll handle
    // all messages from users here.
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
        	console.log((new Date()) + ' New incomming data (Raw DATA): ' + message.utf8Data);
           	
           	var JsonObj = JSON.parse(message.utf8Data);           
           	
           	var MsgType = JsonObj.MsgType;
           	if(MsgType == 'add_score'){
	           	var JsonResult = { };
	           	JsonResult.MsgType = 'add_score_response';
	           	JsonResult.Msg = true;
	           	
	           	var JSONData = JSON.stringify(JsonResult);
	           	connection.sendUTF(JSONData);
	           	
	           	db = new sqlite3.Database(file);
	           	var stmt = db.prepare("INSERT INTO scores VALUES (?, ?, ?, ?)");
	           	stmt.run(JsonObj.Name, JsonObj.Time, JsonObj.Moves, (new Date()));
	           	stmt.finalize();
				
				db.all("SELECT name, time, moves, (time + moves)  AS score FROM scores ORDER BY score LIMIT 10", function(err, rows){
					for (var i=0; i < clients.length; i++) {
                		clients[i].sendUTF(getHighScoreData(rows));
                	}
				});
				
	           	db.close();
           	} else if(MsgType == 'request_highscores'){
	           	db = new sqlite3.Database(file);
				db.all("SELECT name, time, moves, (time + moves)  AS score FROM scores ORDER BY score LIMIT 10", function(err, rows){
					sendHighScoreData(rows);
				});
				db.close();
           	}
        }
    });

    connection.on('close', function(connection) {
	    console.log((new Date()) + " Peer " + connection.remoteAddress + " disconnected.");
        // remove user from the list of connected clients
        clients.splice(index, 1);
    });
	
	function getHighScoreData(rows){
    	var JsonResult = { };
       	JsonResult.MsgType = 'request_highscores_response';
       	JsonResult.Msg = 'Here are the latest highscores!';
       	JsonResult.Data = rows;
       	
	    var JSONString = JSON.stringify(JsonResult);
	    return JSONString;
    }
    
    function sendHighScoreData(rows){
    	var JsonResult = { };
       	JsonResult.MsgType = 'request_highscores_response';
       	JsonResult.Msg = 'Here are the latest highscores!';
       	JsonResult.Data = rows;
       	
	    var JSONString = JSON.stringify(JsonResult);
	    connection.sendUTF(JSONString);
    }
});