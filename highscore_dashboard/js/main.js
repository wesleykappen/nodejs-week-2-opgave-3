window.addEvent("domready", function()
{
	window.WebSocket = window.WebSocket || window.MozWebSocket;
	var connection = new WebSocket('ws://127.0.0.1:1337');
	
	connection.onmessage = function (message)
	{
	    // try to decode json (I assume that each message from server is json)
	    try
		{
	        var json = JSON.parse(message.data);
	        
	        var MsgType = json.MsgType;

	        if (MsgType == 'request_highscores_response')
			{
				handleHighscoreData(json.Data);
	        }
	    }
		catch (e)
		{
	        alert('This doesn\'t look like a valid JSON: ' + message.data);
	        return;
	    }
	    // handle incoming message
	};
	
	connection.onopen = function ()
	{
		requestHighscoreData();
	}
	
	function handleHighscoreData(data)
	{
		// Maak de tbody met id scoreBoardBody leeg.
		$("scoreBoardBody").empty();
		
		var plaats = 1;
		var scoreBoardBody = "";
		JSON.decode(JSON.stringify(data)).each(function(value, key)
		{
			scoreBoardBody = scoreBoardBody + '<tr><td class="center">' + plaats++ + '</td><td class="left">' + value.name + '</td><td class="center">' + value.time + '</td><td class="center">' + value.moves + '</td><td class="center">' + value.score + '</td></tr>';
		});
		$("scoreBoardBody").set('html', scoreBoardBody);
		
		// Maak de tfoot met id scoreBoardFoot leeg.
		$("scoreBoardFoot").empty();
		var scoreBoardFoot = '<tr><td colspan="5" class="center small">Laatste update om ' + new Date().format("%d-%m-%Y %H:%M:%S") + '</td></tr>';
		$("scoreBoardFoot").set('html', scoreBoardFoot);
	}
	
	function requestHighscoreData()
	{
		var obj = { };
		obj.MsgType = 'request_highscores';
		obj.Msg = 'now';
		
		var JSONString = JSON.stringify(obj);
		
		if (connection.readyState === connection.OPEN)
		{
			connection.send(JSONString);
		}
	}
});
